= Mindshare Teams
:page-layout: without_menu

Mindshare Teams are focused on helping our community grow both the number of users and contributors. The teams in Mindshare are each focused on a different aspect of this goal.

* xref:mindshare-committee:ROOT:index.adoc[**Fedora Mindshare Committee**] — The Mindshare Committee fosters communication amongst the Mindshare teams and helps facilitate Council strategy.

* xref:commops:ROOT:index.adoc[**Fedora Community Operations (CommOps)**] — Community Operations (CommOps) provides tools, resources, and utilities for different sub-projects of Fedora to improve effective communication.

* xref:fedora-docs:ROOT:index.adoc[**Fedora Documentation (Docs)**] — The group behind this very documentation site. Includes contributors' guidelines to Fedora Docs

* xref:badges:ROOT:index.adoc[**Fedora Badges**] — The group behind Fedora Badges — a fun website built to recognize contributors to the Fedora Project, help new and existing Fedora contributors find different ways to get involved, and encourage the improvement of Fedora's infrastructure.

* xref:fedora-magazine:ROOT:index.adoc[**Fedora Magazine**] — Guidelines around contributing to the Fedora Magazine.

* xref:websites:ROOT:index.adoc[**Fedora Websites**] - Documentation covering maintenance and expansion of Fedora Project websites.

* xref:fedora-join:ROOT:index.adoc[**Fedora Join SIG**] - The Fedora Join SIG aims to set up and maintain channels that let prospective contributors engage with the community.

* xref:ask-fedora-sops:ROOT:index.adoc[**Ask Fedora SOPs**] - Standard operating procedures for the link:https://ask.fedoraproject.org/[Ask Fedora forums].

* xref:localization:ROOT:index.adoc[**Fedora Localization (l10n)**] - The group of translation community, and people behind translation/l10n tooling and apps, including the https://translate.fedoraproject.org/[Fedora translation platform].

* xref:docs-l10n:ROOT:index.adoc[**Fedora Docs Localization Stats**] - Statistics about the Fedora documentation localization progress.

* xref:marketing::index.adoc[**Fedora Marketing**] - Documentation about the Fedora Marketing team.
